import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Span]';
//
const Span = ({ children, id, className, ...props }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} {...props}>
      {children}
    </StyledComponent>
  );
}

//
Span.propTypes = {
  children: PropTypes.node,
}

Span.defaultProps = {
  id: IdSuFix
}

export default Span;