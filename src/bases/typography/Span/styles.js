import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';

const IdSuFix = '[Span]';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      fontSize: isMobile? '4vmin' : '1vmax',
    },

    empty: {
    }
  })
  (({ classes, children, className, id, ...props
    }) => (
      <span id={id + IdSuFix}
        className={clsx(classes.root, className ? 
          className: classes.empty )} {...props}>
        {children}
      </span>
  ));
