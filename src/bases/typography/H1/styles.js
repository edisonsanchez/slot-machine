import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';

const IdSuFix = '[h1]';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: isMobile? '7vmin' : '3vmax',
      marginBlockStart: 0,
      marginBlockEnd: 0,
      textAlign: 'center'
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      clone }) => (
      <h1 id={id + IdSuFix}
        className={clsx(classes.root, className ? 
          className: classes.empty )}>
        {children}
      </h1>
  ));
