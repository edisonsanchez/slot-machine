import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.H1]';
//
const H1 = ({ children, id, className,
    clone }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className}>
      {children}
    </StyledComponent>
  );
}

//
H1.propTypes = {
  children: PropTypes.node
}

H1.defaultProps = {
  id: IdSuFix
}

export default H1;