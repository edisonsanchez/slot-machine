import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    fixed: {

    },

    empty: {
    }
  })
  (({ classes, children, className, id, props,
      clone }) => (
      <form id={id}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
        {...props}>
        {children}
      </form>
  ));
