import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Form]';

//
const Form = ({ children, id, className, props }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className}
      props>
      {children}
    </StyledComponent>
  );
}

//
Form.propTypes = {
  children: PropTypes.node
}

Form.defaultProps = {
  id: IdSuFix,
}

export default Form;