import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Img]';
//
const Img = ({ id, className, 
  src, alt }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      src={src}
      alt={alt}
      />
  );
}

//
Img.propTypes = {
  children: PropTypes.node,
}

Img.defaultProps = {
  id: IdSuFix
}

export default Img;