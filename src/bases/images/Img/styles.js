import React from 'react';
import clsx from 'clsx';
// import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';

const IdSuFix = '[img]';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      margin: 0,
      padding: 0,
      width: '100%',
      height: '100%'
    },

    empty: {
    }
  })
  (({ classes, className, id, ...props
   }) => (
      <img
        id={id + IdSuFix}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
        alt={props.alt}
        {...props}></img>
  ));
