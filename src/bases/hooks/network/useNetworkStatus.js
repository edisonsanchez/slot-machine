import useOnlineStatus from '@rehooks/online-status';


export const useNetworkStatus = () => {
  const online = useOnlineStatus();

  return { online };
}
