import useNetworkStatus from "@rehooks/network-status"

export const useNetworkStats = () => {
  const { effectiveType, saveData, rtt, downlink } = useNetworkStatus();

  return { effectiveType, saveData, rtt, downlink };
}
