import useInputValueRehooks from '@rehooks/input-value';


export const useInputValue = initialValue => {
  return useInputValueRehooks(initialValue);
}
