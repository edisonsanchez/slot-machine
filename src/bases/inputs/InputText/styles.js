import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import MUIInputText from '@material-ui/core/TextField';

const IdSuFix = '[MUIInputText]';

//
export const StyledComponent = withStyles({
    root: {
      height: '15%',
      display: 'flex',
      justifyContent: 'center'
    },

    focused: {
    },

    disabled: {
    },
    
    colorSecondary: {
    },
    
    underline: {
    },

    error: {
    },

    multiline: {
    },

    fullWidth: {
    },

    input: {
      // padding: 0,
      padding: '2vmin',
      // paddingRight: '2vmin',
      fontSize: isMobile ? '5vmin' : '1.5vmax',
    },

    inputMarginDense: {
    },

    inputMultiline: {
    },

    inputTypeSearch: {
    },

    empty: {
    }
  })
  (({ classes, className, id, 
      autoComplete, autoFocus, defaultValue, disabled, inputProps, 
      error, fullWidth, inputRef, multiline, name,
      onChange, placeholder, required, rows, rowsMax, 
      type, value }) => (
      <MUIInputText id={id + IdSuFix}
        className={clsx(classes.root,
          className ? className: classes.empty )}
        autoComplete={autoComplete} autoFocus={autoFocus} defaultValue={defaultValue} 
        disabled={disabled} inputProps={{className: classes.input, ...inputProps}}
        error={error} fullWidth={fullWidth} inputRef={inputRef} multiline={multiline}
        name={name} onChange={onChange} placeholder={placeholder} required={required}
        rows={rows} rowsMax={rowsMax} type={type} value={value} />
  ));
  // , classes.focus, classes.disabled, classes.colorSecondary, 
  //         classes.underline, classes.error, classes.multiline, classes.fullWidth, classes.input, 
  //         classes.inputMarginDense, classes.inputMultiline, classes.inputTypeSearch,
