import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.InputText]';

//
export const InputText = (props) => {

    return (
    <StyledComponent 
      id={props.id + IdSuFix}
      className={props.className} 
      {...props} />
  );
}

//
InputText.propTypes = {
  placeholder: PropTypes.string,
}

InputText.defaultProps = {
  id: IdSuFix,
}

export default InputText;