import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import MUIComponent from '@material-ui/core/IconButton';

//
export const StyledComponent = withStyles({
    root: {
      cursor: 'pointer',
      backgroundColor: 'transparent',
      color: 'blue',
      textDecoration: 'underline',
      fontSize: isMobile?  '3vmin' : '2vmin',
      padding: 0,
      paddingLeft: isMobile? '2vmin' : '1vmax',
    },

    label: {

    },

    text: {

    },

    textPrimary: {

    },

    textSecondary: {

    },

    outlined: {

    },

    outlinedPrimary: {

    },

    outlinedSecondary: {

    },

    disabled: {

    },

    startIcon: {

    },

    endIcon: {

    },

    empty: {
    }
  })
  (({ classes, children, className, id,
      ...props }) => (
      <MUIComponent id={`${id}[B.ButtonIcon]`} 
        className={clsx(classes.root, className? className: classes.empty)}
        {...props}
        >
        {children}
      </MUIComponent>
  ));
