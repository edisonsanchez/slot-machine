import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Link]'

//
const ButtonIcon = ({ children, id, className, styles, ...props }) => {

    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      onClick={props.onClick} 
      styles={styles}
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
ButtonIcon.propTypes = {
  children: PropTypes.node
}
//
ButtonIcon.defaultProps = {
  id: IdSuFix
}

export default ButtonIcon;