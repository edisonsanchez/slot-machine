import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Link]'

//
const Link = ({ children, id, className, styles, onClick, 
    startIcon, disabled, disableElevation, endIcon,
    fullWidth, variant, color }) => {

    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      onClick={onClick} 
      styles={styles}
      startIcon={startIcon} disabled={disabled} disableElevation={disableElevation}
      endIcon={endIcon} fullWidth={fullWidth} variant={variant} color={color}>
      {children}
    </StyledComponent>
  );
}

//
Link.propTypes = {
  children: PropTypes.node
}
//
Link.defaultProps = {
  id: IdSuFix
}

export default Link;