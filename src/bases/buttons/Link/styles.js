import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import MUIButton from '@material-ui/core/Button';

const IdSuFix = '[MUILink]';

//
export const StyledComponent = withStyles({
    root: {
      cursor: 'pointer',
      backgroundColor: 'transparent',
      color: 'blue',
      textDecoration: 'underline',
      fontSize: isMobile?  '3vmin' : '2vmin',
      padding: 0,
      paddingLeft: isMobile? '2vmin' : '1vmax',
    },

    label: {

    },

    text: {

    },

    textPrimary: {

    },

    textSecondary: {

    },

    outlined: {

    },

    outlinedPrimary: {

    },

    outlinedSecondary: {

    },

    disabled: {

    },

    startIcon: {

    },

    endIcon: {

    },

    empty: {
    }
  })
  (({ classes, children, className, onClick, id,
      startIcon, disabled, disableElevation, endIcon,
      fullWidth, variant, color }) => (
      <MUIButton id={id + IdSuFix}
        onClick={onClick} 
        className={clsx(classes.root, className? className: classes.empty)}
        startIcon={startIcon} disabled={disabled} disableElevation={disableElevation}
        endIcon={endIcon} fullWidth={fullWidth} variant={variant} color={color}>
        {children}
      </MUIButton>
  ));
