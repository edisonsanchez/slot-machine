import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Button]'

//
const Button = ({ children, id, className, styles, onClick, 
    startIcon, disabled, disableElevation, endIcon,
    fullWidth, variant, color }) => {

    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      onClick={onClick} 
      styles={styles}
      startIcon={startIcon} disabled={disabled} disableElevation={disableElevation}
      endIcon={endIcon} fullWidth={fullWidth} variant={variant} color={color}>
      {children}
    </StyledComponent>
  );
}

//
Button.propTypes = {
  children: PropTypes.node
}
//
Button.defaultProps = {
  id: IdSuFix
}

export default Button;