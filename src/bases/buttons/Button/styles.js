import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIButton from '@material-ui/core/Button';

const IdSuFix = '[MUIButton]';

//
export const StyledComponent = withStyles({
    root: {
      cursor: 'pointer',
      backgroundColor: 'black',
      color: 'white',
      width: '100%',

      '&:hover': {
        backgroundColor: '#707070',
        borderColor: '#A3ADD0',
        borderRadius: 5,
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 1px rgba(80,80,80,.5)',
      },
    },

    label: {

    },

    text: {

    },

    textPrimary: {

    },

    textSecondary: {

    },

    outlined: {

    },

    outlinedPrimary: {

    },

    outlinedSecondary: {

    },

    disabled: {

    },

    startIcon: {

    },

    endIcon: {

    },

    empty: {
    }
  })
  (({ classes, children, className, onClick, id,
      startIcon, disabled, disableElevation, endIcon,
      fullWidth, variant, color }) => (
      <MUIButton id={id + IdSuFix}
        onClick={onClick} 
        className={clsx(classes.root, className? className: classes.empty)}
        startIcon={startIcon} disabled={disabled} disableElevation={disableElevation}
        endIcon={endIcon} fullWidth={fullWidth} variant={variant} color={color}>
        {children}
      </MUIButton>
  ));
