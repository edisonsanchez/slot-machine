import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIBox from '@material-ui/core/Box';

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    fixed: {

    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      clone }) => (
      <MUIBox id={id}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
        clone={clone}>
        {children}
      </MUIBox>
  ));
