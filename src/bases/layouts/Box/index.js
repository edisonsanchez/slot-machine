import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Box]';

//
const Box = ({ children, id, className,
    clone }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      clone={clone}>
      {children}
    </StyledComponent>
  );
}

//
Box.propTypes = {
  children: PropTypes.node,
  clone: PropTypes.bool
}

Box.defaultProps = {
  id: IdSuFix,
  clone: false
}

export default Box;