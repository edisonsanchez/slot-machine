import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIContainer from '@material-ui/core/Container';

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100vw',
    },

    fixed: {

    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      disableGutters, fixed }) => (
      <MUIContainer id={id}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
        disableGutters={disableGutters} fixed={fixed}>
        {children}
      </MUIContainer>
  ));
