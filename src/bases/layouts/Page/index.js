import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.Page]';

//
const Page = ({ children, id, className,
    disableGutters, fixed }) => {

    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} 
      disableGutters={disableGutters} 
      fixed={fixed}>
      {children}
    </StyledComponent>
  );
}

//
Page.propTypes = {
  children: PropTypes.node
}

Page.defaultProps = {
  id: IdSuFix,
  disableGutters: true,
  fixed: false
}

export default Page;