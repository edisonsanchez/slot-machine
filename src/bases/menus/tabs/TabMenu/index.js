import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const TabMenu = ({ children, id, className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id}
      className={className}
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
TabMenu.propTypes = {
  children: PropTypes.node,
}

TabMenu.defaultProps = {
  id: ''
}

export default TabMenu;