import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIComponent from '@material-ui/core/Tabs';

const ID = '<B.TabMenu>';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    empty: {
    }
  })
  (({ classes, children, className, id = '', 
      ...props }) => (
      <MUIComponent id={`${id}${ID}`}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
          {...props}>
        {children}
      </MUIComponent>
  ));
