import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const TabMenuItem = ({ id, className,
    ...props }) => {

    return (
    <StyledComponent 
      id={id}
      className={className}
      {...props} />
  );
}

//
TabMenuItem.propTypes = {
  id: PropTypes.string
}

TabMenuItem.defaultProps = {
  id: ''
}

export default TabMenuItem;