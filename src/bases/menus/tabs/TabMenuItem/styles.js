import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIComponent from '@material-ui/core/Tab';

const ID = '<B.TabMenuItem>';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      ...props }) => (
      <MUIComponent id={`${id}${ID}`}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
          {...props} />
  ));
