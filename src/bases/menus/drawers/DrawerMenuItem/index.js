import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const DrawerMenuItem = ({ children, id, className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id}
      className={className} 
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
DrawerMenuItem.propTypes = {
  children: PropTypes.node,
}

DrawerMenuItem.defaultProps = {
  id: ''
}

export default DrawerMenuItem;