import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import MUICompoment from '@material-ui/core/MenuItem';

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      fontSize: isMobile? '7vmin' : '3vmax',
      marginBlockStart: 0,
      marginBlockEnd: 0,
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      ...props }) => (
      <MUICompoment id={`${id}[B.DrawerMenuItem]`}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
          {...props}>
        {children}
      </MUICompoment>
  ));
