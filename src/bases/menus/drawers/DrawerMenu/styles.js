import React from 'react';
import clsx from 'clsx';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MUIComponent from '@material-ui/core/Drawer';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  }
}));

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      ...props }) => {
      const _classes = useStyles();

      return (
      <MUIComponent id={`${id}[B.DrawerMenu]`}
        className={clsx(classes.root, className ? 
          className : classes.empty, 
          {
            [_classes.drawerOpen]: props.open,
            [_classes.drawerClose]: !props.open,
          } 
        )}
        classes={{
          paper: clsx({
            [_classes.drawerOpen]: props.open,
            [_classes.drawerClose]: !props.open,
          }),
        }}
          {...props}>
        {children}
      </MUIComponent>
  )});
