import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'


//
const DrawerMenu = ({ children, id = '', className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id}
      className={className}
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
DrawerMenu.propTypes = {
  children: PropTypes.node,
}

DrawerMenu.defaultProps = {
  id: ''
}

export default DrawerMenu;