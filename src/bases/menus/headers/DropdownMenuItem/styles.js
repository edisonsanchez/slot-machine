import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';

const IdSuFix = '[MUIMenuItem]';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      fontSize: isMobile? '7vmin' : '3vmax',
      marginBlockStart: 0,
      marginBlockEnd: 0,
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      ...props }) => (
      <MenuItem id={id + IdSuFix}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
          {...props}>
        {children}
      </MenuItem>
  ));
