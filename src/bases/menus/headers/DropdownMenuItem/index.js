import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.DropdownMenuItem]';
//
const DropdownMenuItem = ({ children, id, className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className} {...props}>
      {children}
    </StyledComponent>
  );
}

//
DropdownMenuItem.propTypes = {
  children: PropTypes.node,
}

DropdownMenuItem.defaultProps = {
  id: IdSuFix
}

export default DropdownMenuItem;