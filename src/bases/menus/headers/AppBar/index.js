import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
export const AppBar = ({ children, id, className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id}
      className={className}
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
AppBar.propTypes = {
  children: PropTypes.node,
}

AppBar.defaultProps = {
  id: ''
}

export default AppBar;