import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import MUIComponent from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      position="static",
      ...props }) => (
      <MUIComponent id={`${id}.[B.AppBar]`}
        className={clsx(classes.root, className ? 
          className: classes.empty )}

          {...props}>
        <Toolbar>
          {children}
        </Toolbar>
      </MUIComponent>
  ));
