import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';

const IdSuFix = '[MuiMenu]';
//
export const StyledComponent = withStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    empty: {
    }
  })
  (({ classes, children, className, id, 
      ...props }) => (
      <Menu id={id + IdSuFix}
        className={clsx(classes.root, className ? 
          className: classes.empty )}
          {...props}>
        {children}
      </Menu>
  ));
