import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

const IdSuFix = '[B.DropdownMenu]';
//
const DropdownMenu = ({ children, id, className,
    ...props }) => {
    return (
    <StyledComponent 
      id={id + IdSuFix}
      className={className}
      {...props}>
      {children}
    </StyledComponent>
  );
}

//
DropdownMenu.propTypes = {
  children: PropTypes.node,
}

DropdownMenu.defaultProps = {
  id: IdSuFix
}

export default DropdownMenu;