import React from 'react'
import PropTypes from 'prop-types'
import PageContainer from '../PageContainer'
import { Stylesheet } from './styles';
// import { View } from 'components/views'
// import { AdminBar } from 'components/bars/AdminBar'
import { AdminMenu } from 'components/bars/AdminMenu'
import { useHistory } from "react-router-dom";
import { useAuthContext } from 'contexts/AuthContext'

//Functional Class Component
export const AdminLayout = ({children, id = '', metaTitle, metaDescription}) => {
  const history = useHistory();
  const { authLogout } = useAuthContext();


  const handleMenuItem = (option) => {
    console.log(`handleMenuItem option: ${option}`);

    switch(option){ 
      case 'machines':
        console.log('Selected Machines')
        history.push("/machines");
        break;

      case 'stores':
        history.push("/stores");
        break;

      case 'agents':
        history.push("/agents");
        break;

      case 'users':
        history.push("/users");
        break;

      case 'logout':
        authLogout();
        history.push("/");
        break;

      default: 
        break;
    }
  }

  return (
    <PageContainer id={`${id}.AdminLayout.PageContainer`} 
      metaTitle={metaTitle} metaDescription={metaDescription}
      styles={Stylesheet.pageContainer}>
        <AdminMenu onClick={handleMenuItem}>
          {children}
        </AdminMenu>
    </PageContainer>
  )
}

//
AdminLayout.propTypes = {
  children: PropTypes.node
}
//
AdminLayout.defaultProps = {
  id: ''
}
  
  export default AdminLayout;