import React from 'react'
import PropTypes from 'prop-types'

import MetaData from '../MetaData'
import { StyledComponent } from './styles'
// import Log from 'functions/Log'

//
const IdSuFix = '.PageContainer';

//Functional Class Component
export const PageContainer = ({children, id, metaTitle, metaDescription, styles}) => {

  return (
    <StyledComponent id={id + IdSuFix} disableGutters={true} styles={styles}>
      <MetaData title={metaTitle} description={metaDescription}/>
      {children}
    </StyledComponent>
  )
}

//
PageContainer.propTypes = {
  children: PropTypes.node,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string
}
//
PageContainer.defaultProps = {
  id: IdSuFix
}
  
  export default PageContainer;