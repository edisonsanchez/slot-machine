import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/layouts/Page';

const IdSuFix = '[C.Page]'

// Local Styles
const styles = {
  root: {
    // 
  },

  fixed: {
    // 
  }
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    fixed:  props => ( props.styles && props.styles.fixed 
      ? { ...styles.fixed,
          ...props.styles.fixed 
      } : styles.fixed),

    empty: {}

  })
  (({ classes, children, id,
      disableGutters, fixed }) => (
      <BaseComponent id={id + IdSuFix}
        className={clsx(classes.root, classes.fixed)}
        disableGutters={disableGutters} fixed={fixed} >
        {children}
      </BaseComponent>
  ));
