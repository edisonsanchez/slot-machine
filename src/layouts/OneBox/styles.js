import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/layouts/Box';

const IdSuFix = '[C.Page]'

// Local Styles
const styles = {
  root: {
    // 
  },

  fixed: {
    // 
  }
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    empty: {}

  })
  (({ classes, children, id,
      ...props }) => (
      <BaseComponent id={id + IdSuFix}
        className={clsx(classes.root, classes.fixed)}
        {...props} >
        {children}
      </BaseComponent>
  ));
