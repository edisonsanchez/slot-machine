import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.OneBox';

//Functional Class Component
export const OneBox = ({children, id, styles}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles}>
      {children}
    </StyledComponent>
  )
}

//
OneBox.propTypes = {
  children: PropTypes.node,
}
//
OneBox.defaultProps = {
  id: IdSuFix
}
  
  export default OneBox;