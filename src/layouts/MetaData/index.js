import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import Constants from 'data/constants'

//Functional Class Component
export const MetaData = ({title, description}) => {
  return (
    <Helmet>
      {title && <title>{`${title} | ${Constants.metaTitle}`}</title>}
      {description && <meta name='description' content={description} /> }
    </Helmet>
  )
}

//
MetaData.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
}
//
MetaData.defaultProps = {
  description: Constants.metaDescription
}

export default MetaData;
