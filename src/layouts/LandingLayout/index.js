import React from 'react'
import PropTypes from 'prop-types'
import PageContainer from '../PageContainer'
import { Stylesheet } from './styles';

//
const IdSuFix = 'LandingLayout';

//Functional Class Component
export const LandingLayout = ({children, id, metaTitle, metaDescription}) => {
   
  return (
    <PageContainer id={`${id}.${IdSuFix}.PageContainer`} 
      metaTitle={metaTitle} metaDescription={metaDescription}
      styles={Stylesheet.pageContainer}>
        {children}
    </PageContainer>
  )
}

//
LandingLayout.propTypes = {
  children: PropTypes.node
}
//
LandingLayout.defaultProps = {
  id: IdSuFix
}
  
  export default LandingLayout;