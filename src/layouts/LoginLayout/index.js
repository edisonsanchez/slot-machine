import React from 'react'
import PropTypes from 'prop-types'
import PageContainer from '../PageContainer'
import OneBox from '../OneBox'
import { Stylesheet } from './styles';

//
const IdSuFix = '.LoginLayout';

//Functional Class Component
export const LoginLayout = ({children, id, metaTitle, metaDescription}) => {
   
  return (
    <PageContainer id={id + IdSuFix} metaTitle={metaTitle} metaDescription={metaDescription}
      styles={Stylesheet.pageContainer}>
      <OneBox id={id+IdSuFix} styles={Stylesheet.oneBox}>
        {children}
      </OneBox>
    </PageContainer>
  )
}

//
LoginLayout.propTypes = {
  children: PropTypes.node
}
//
LoginLayout.defaultProps = {
  id: IdSuFix
}
  
  export default LoginLayout;