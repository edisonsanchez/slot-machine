import { isMobile } from 'react-device-detect'

//
export const Stylesheet = {
  pageContainer: {
    root: {
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh',
    }
  },
    
  oneBox: {
    root: {
      width: !isMobile? '40vw' : '90vw',
      minWidth: !isMobile? 250 : '90vw',
      maxWidth: !isMobile? 500 : '90vw',
      height: !isMobile? '60vh' : '70vh',
      minHeight: !isMobile? '30vw' : '70vh' ,
      justifyContent: 'space-between',
      padding: '2vmin',
    }
  }
};