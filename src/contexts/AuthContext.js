/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, createContext } from 'react';
import { Amplify, Auth, Hub } from 'aws-amplify';
import aws_exports from 'aws-exports';
//
// import { cognitoSignin, federatedSignin, 
//   cognitoSignup, cognitoConfirm, cognitoForgot } from 'handlers/Cognito'
import { cognitoSignin } from 'handlers/Cognito/cognitoSignin';
import { federatedSignin } from 'handlers/Cognito/federatedSignin';
import { cognitoSignup } from 'handlers/Cognito/cognitoSignup';
import { cognitoConfirm } from 'handlers/Cognito/cognitoConfirm';
import { cognitoForgot } from 'handlers/Cognito/cognitoForgot'

//Context
export const AuthContext = createContext(null);

//Provider
export const AuthContextProvider = ({ children }) => {
  const [isAuthenticating, setIsAuthenticating] = React.useState(true);
  const [isAuthenticated, userHasAuthenticated] = React.useState(false);
  const [user, setUser] = React.useState(null);
  const [userData, setUserData] = React.useState(null);

  //
  const [loadingAuth, setLoading] = React.useState(true);
  const [data, setData] = React.useState(null);
  const [error, setError] = React.useState(null);
  //
  const setDataErrorLoading = ( data, error, _loading ) => {
    setData(data);
    setError(error);

    if( _loading === undefined || _loading === null ){
      setLoading(_loading);
    }
  };

  //ComponentDidMouunt
  React.useEffect(() => {
    setLoading(true);

    //Configure
    Amplify.configure(aws_exports);

    //HUB Listen
    Hub.listen('auth', async (data) => {
      const { payload } = data

      if (payload.event === 'signIn') {
        setLoading(true);
        userHasAuthenticated(true);

        try{
          const data = await Auth.currentAuthenticatedUser();
          authSetUserData(data);
          userHasAuthenticated(true);

          setDataErrorLoading( data, null, false);
        } catch (err) {
          userHasAuthenticated(false);

          setDataErrorLoading( null, err, false);
        }

      } else if (payload.event === 'signOut') {
        setLoading(true);
        userHasAuthenticated(false);
        setDataErrorLoading( null, null, false);
      }
    }); //HUB.

    //Inner OnLoad only in ComponentDidMount
    const onLoad = async () => {
      try {
        const data = await Auth.currentAuthenticatedUser();

        console.log('onLoad');
        console.log(data);
        authSetUserData(data);
        userHasAuthenticated(true);
      } catch (err) {
        userHasAuthenticated(false);
      }
    }

    onLoad();
    setIsAuthenticating(false);
    setLoading(false);
  }, []);

  //Save User and Attributes
  const authSetUserData = ( authUserData ) => {
    let name = authUserData 
      ? authUserData.attributes
        ? authUserData.attributes.name
          ? authUserData.attributes.name
          : authUserData.attributes.email
            ? authUserData.attributes.email.split('@')[0]
            : 'Name'
        : 'Loading...'
      : 'Loading...';

    let attributes = {
      ...authUserData.attributes,

      name: name,
    };

    setUser(authUserData);
    setUserData(attributes);
  }

  //
  const authLogout = async () => {
    setLoading(true);

    try{
      const data = await Auth.signOut();

      setDataErrorLoading( data, null );
    } catch (err){
      setDataErrorLoading( null, err );
    }

    userHasAuthenticated(false);
    setLoading(false);
  }

  //
  const authFederated = async (type) => {
    setLoading(true);
    const result = await federatedSignin(type);
    setLoading(result.loading);
    return result;
  }

  //
  const authLogin = async (username, password) => {
    setLoading(true);
    const result = await cognitoSignin( username, password);
    console.log('authLogin');
    console.log(result);
    // setDataErrorLoading( result.data,  , false );

    if( result.data ){
      userHasAuthenticated(true);
      authSetUserData(result.data)
    }

    const response = {
      data: result.data,
      error: result.error ? result.error.message : null,
    }

    return response;
  }

  // Submit button on Login Form.
  const authSignup = async ( username, password ) => {
    setLoading(true);
    const result = await cognitoSignup( username, password );
    setDataErrorLoading( result.data, result.error, false );

    return result;
  }


  // Submit button on Login Form.
  const authConfirm = async (username, password, code) => {
    setLoading(true);
    const result = await cognitoConfirm( username, code);

    if ( result.data && password) {
      const resultLogin = await cognitoSignin( username, password );
      userHasAuthenticated(true);
      authSetUserData(resultLogin.data)

      setDataErrorLoading( resultLogin.data, resultLogin.error, false );
      return resultLogin;
    }

    setDataErrorLoading( result.data, result.error, false );
    return result;
  }

  //
  const authForgot = async ( username, password, code ) => {
    setLoading(true);
    const result = await cognitoForgot( username, password, code );
    setDataErrorLoading( result.data, result.error, false );
    return result;
  }

  //
  const values = React.useMemo(() => (
    { data, error, loadingAuth, 
      user, userData, isAuthenticating, isAuthenticated, 
      userHasAuthenticated, setIsAuthenticating,
      authLogout, authFederated, authLogin, authSignup, authConfirm, authForgot }), 
    [ data, error, loadingAuth, 
      user, userData, isAuthenticating, isAuthenticated ]);

  // Finally, return the interface that we want to expose to our other components
  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
}

//
export function useAuthContext() {
  const context = useContext(AuthContext);
  return context;
}