import React from 'react';
import { useContext, createContext } from "react";
import { useTranslation } from 'react-i18next'
import '../containers/localize/i18n'

export const LanguageContext = createContext(() => {
  const { i18n } = useTranslation()
  i18n.changeLanguage(localStorage.getItem('local.language'))
});

export const LanguageContextProvider = ({ children }) => {
  const [ init, setInit ] = React.useState(false);
  const [isLoadingLanguage, setLoading] = React.useState(true);
  const [language, setLanguage] = React.useState(()=>{
    const x = localStorage.getItem('local.language')
    return x? x: 'en'
  });
  const { t, i18n } = useTranslation()

  //
  const constructor = () => {

    if( !init ){

      if(language === null){
        setLanguage('en')
        console.log('setLanguage')
        console.log(language)

        if(language){
          localStorage.setItem('local.language', language);
        }
        else
          localStorage.setItem('local.language', 'en');
      }
        
      setInit(true);
    }
  }
  constructor();

  //Component Did Mount
  React.useEffect(() => {
    const localStorageLanguage = async () => {
        await i18n.changeLanguage(language, () => {setLoading(false)});
    }

    localStorageLanguage();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  //On Update language
  React.useEffect(() => {
    localStorage.setItem('local.language', language);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [language]);


  const onChange = async ( lang ) => {
    await i18n.changeLanguage(lang, () => {setLanguage(lang); return lang;});
  }

  //.
  const translate = ( text ) => {

    if(i18n.language !== language){
      i18n.changeLanguage(language, () => { return t(text) });
      return;
    }

    const retValue = t(text);
    return (retValue? retValue : '');
  }

  //
  const values = React.useMemo(() => (
    { language, isLoadingLanguage, onChange, translate }), 
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [ language, isLoadingLanguage ]);

  // Finally, return the interface that we want to expose to our other components
  return <LanguageContext.Provider value={values}>{children}</LanguageContext.Provider>;

}

export function useLanguageContext() {
  const context = useContext(LanguageContext);

  return context;
}