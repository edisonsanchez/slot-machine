import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.View';

//Functional Class Component
export const View = ({children, id, styles}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles}>
      {children}
    </StyledComponent>
  )
}

//
View.propTypes = {
  children: PropTypes.node,
}
//
View.defaultProps = {
  id: IdSuFix
}
  
  export default View;