import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/layouts/Box';

const IdSuFix = '[C.Box]'

// Local Styles
const styles = {
  root: {
    flexDirection: 'row',
  },
};

//Classes Component
export const StyledComponent = withStyles({
  root:  props => ( props.styles && props.styles.root 
    ? { ...styles.root, ...props.styles.root } : styles.root),

  empty: {}

  })
  (({ classes, children, id }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root, classes.fixed)}>
      {children}
    </BaseComponent>
  ));
