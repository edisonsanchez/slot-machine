import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.Row';

//Functional Class Component
export const Row = ({children, id, styles}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles}>
      {children}
    </StyledComponent>
  )
}

//
Row.propTypes = {
  children: PropTypes.node,
}
//
Row.defaultProps = {
  id: IdSuFix
}
  
  export default Row;