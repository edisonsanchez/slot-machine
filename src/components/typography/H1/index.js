import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.H1';

//Functional Class Component
export const H1 = ({children, id, styles}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles}>
      {children}
    </StyledComponent>
  )
}

//
H1.propTypes = {
  children: PropTypes.node,
}
//
H1.defaultProps = {
  id: IdSuFix
}
  
  export default H1;