import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.Text';

//Functional Class Component
export const Text = ({children, id, styles}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles}>
      {children}
    </StyledComponent>
  )
}

//
Text.propTypes = {
  children: PropTypes.node,
}
//
Text.defaultProps = {
  id: IdSuFix
}
  
  export default Text;