import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'


//Functional Class Component
export const TabsMenu = ({children, id, styles, ...props}) => {
  return (
    <StyledComponent id={id} 
      styles={styles} 
      {...props}>
      {children}
    </StyledComponent>
  )
}

//
TabsMenu.propTypes = {
  children: PropTypes.node
}
//
TabsMenu.defaultProps = {
  id: ''
}
  
  export default TabsMenu;