import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'


//Functional Class Component
export const TabMenuItem = ({id, styles, ...props}) => {

  return (
    <StyledComponent id={id} styles={styles} {...props} />
  )
}

//
TabMenuItem.propTypes = {
  id: PropTypes.string,
}
//
TabMenuItem.defaultProps = {
  id: ''
}
  
  export default TabMenuItem;