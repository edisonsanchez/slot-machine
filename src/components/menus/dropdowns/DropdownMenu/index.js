import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.Menu';

//Functional Class Component
export const DropdownMenu = ({children, id, styles, ...props}) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles} {...props}>
      {children}
    </StyledComponent>
  )
}

//
DropdownMenu.propTypes = {
  children: PropTypes.node
}
//
DropdownMenu.defaultProps = {
  id: IdSuFix
}
  
  export default DropdownMenu;