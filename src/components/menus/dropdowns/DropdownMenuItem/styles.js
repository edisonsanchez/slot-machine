import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/menus/dropdowns/DropdownMenuItem';

const IdSuFix = '[C.H1]'

// Local Styles
const styles = {
  root: {
  },
};

//Classes Component
export const StyledComponent = withStyles({
  root:  props => ( props.styles && props.styles.root 
    ? { ...styles.root, ...props.styles.root } : styles.root),

  empty: {}

  })
  (({ classes, children, id, onClick }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root)}
      onClick={onClick}>
      {children}
    </BaseComponent>
  ));
