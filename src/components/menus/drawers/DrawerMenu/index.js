import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

import { ButtonDrawerClose } from 'components/buttons/presets/ButtonDrawerClose';
import Divider from '@material-ui/core/Divider';

//Functional Class Component
export const DrawerMenu = ({children, id = '', styles, ...props}) => {
  return (
    <StyledComponent id={id} styles={styles} {...props}>
      <ButtonDrawerClose onClick={() => {console.log('CLOSE'); props.onClose() }}/>
      <Divider />
      {children}
    </StyledComponent>
  )
}

//
DrawerMenu.propTypes = {
  children: PropTypes.node
}
//
DrawerMenu.defaultProps = {
  id: ''
}

export default DrawerMenu;