import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.DropdownMenuItem';

//Functional Class Component
export const DropdownMenuItem = React.forwardRef(({children = '', id, styles, ...props}, ref) => {
  return (
    <StyledComponent id={id + IdSuFix} styles={styles} {...props}>
      {children}
    </StyledComponent>
  )
})

//
DropdownMenuItem.propTypes = {
  children: PropTypes.node,
}
//
DropdownMenuItem.defaultProps = {
  id: IdSuFix
}
  
  export default DropdownMenuItem;