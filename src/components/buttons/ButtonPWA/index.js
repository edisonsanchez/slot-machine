import React, { useEffect, useState } from "react";
import { useLanguageContext } from 'contexts/LanguageContext';
import { Button } from 'components/buttons'
import { Stylesheet } from './styles'
import useIsIOS from 'hooks/useIsIOS';
import { InstallPWA } from "containers/pwa/InstallPWA";


export const ButtonInstallPWA = () => {
  const [supportsPWA, setSupportsPWA] = useState(false);
  const [promptInstall, setPromptInstall] = useState(null);
  const { translate } = useLanguageContext();
  const { prompt } = useIsIOS();

  useEffect(() => {
    const handler = e => {
      e.preventDefault();
      console.log("we are being triggered :D");
      setSupportsPWA(true);
      setPromptInstall(e);
    };

    console.log('useInstallPWA');
    window.addEventListener("beforeinstallprompt", handler);

    return () => window.removeEventListener("transitionend", handler);
  }, []);

  const onClick = evt => {
    evt.preventDefault();
    if (!promptInstall) {
      return;
    }
    promptInstall.prompt();
  };

  if (!supportsPWA) {
    console.log('not supported');
    if(prompt) {
      return <InstallPWA />;
    }

    return null;
  }

  return (
    supportsPWA &&
      <Button id={'Install-App'} 
        styles={Stylesheet.button}
        onClick={onClick}>
        {translate('Install App')}
      </Button>
  );
};

export default ButtonInstallPWA;