import React from 'react';
import PropTypes from 'prop-types';
import { StyledComponent } from './styles';

//
export const ButtonIcon = ({ children, id = '', styles, ...props}) => {

  console.log('ButtonIcon')
  console.log(props)

  return ( 
    <StyledComponent
        id={id}
        styles={styles}
        {...props}
    >
      {children}
    </StyledComponent>
  );
}

//
ButtonIcon.propTypes = {
  children: PropTypes.node
}

export default ButtonIcon;