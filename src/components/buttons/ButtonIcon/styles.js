import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseButton from 'bases/buttons/ButtonIcon';

// Local Styles
const styles = {
  root: {

  }
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    label: {

    },

    text: {

    },

    textPrimary: {

    },

    textSecondary: {

    },

    outlined: {

    },

    outlinedPrimary: {

    },

    outlinedSecondary: {

    },

    disabled: {

    },

    startIcon: {

    },

    endIcon: {
    },

    styles: props => ({
        ...props.styles
    }),

    empty: {
    }
  })(({ classes, children, onClick, id,
      ...props}) => (
      <BaseButton id={`${id}[C.ButtonIcon]`}
        onClick={onClick} 
        className={clsx(classes.root, classes.styles)}
        {...props}>
        {children}
      </BaseButton>
  ));
