import React from "react";

import { ButtonIcon } from 'components/buttons/ButtonIcon'
import MenuIcon from '@material-ui/icons/Menu';
import { Stylesheet } from './styles'

export const ButtonIconMenu = ({edge="start", color='inherit', label='', ...props}) => {

  return (
    <ButtonIcon id={`${props.id}.ButtonIcon.MenuIcon`}
      edge={edge} 
      color={color}
      aria-label={props.label}
      styles={Stylesheet.button}
      {...props}>
      <MenuIcon />
    </ButtonIcon>
  );
};

export default ButtonIconMenu;