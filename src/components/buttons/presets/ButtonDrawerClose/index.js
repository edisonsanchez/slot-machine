import React from "react";
import { ButtonIcon } from 'components/buttons/ButtonIcon'
import CloseIcon from '@material-ui/icons/Close';
import { Stylesheet } from './styles'

export const ButtonDrawerClose = ({edge="start", color='inherit', label='', ...props}) => {

  return (
    <ButtonIcon id={`${props.id}.ButtonIcon.CloseIcon`}
      edge={edge} 
      color={color}
      aria-label={props.label}
      styles={Stylesheet.button}
      {...props}>
      <CloseIcon />
    </ButtonIcon>
  );
};

export default ButtonDrawerClose;