import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseButton from 'bases/buttons/Link';

const IdSuFix = '[C.Link]'

// Local Styles
const styles = {
  root: {

  }
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    label: {

    },

    text: {

    },

    textPrimary: {

    },

    textSecondary: {

    },

    outlined: {

    },

    outlinedPrimary: {

    },

    outlinedSecondary: {

    },

    disabled: {

    },

    startIcon: {

    },

    endIcon: {
    },

    styles: props => ({
        ...props.styles
    }),

    empty: {
    }
  })(({ classes, children, onClick, id,
    startIcon, disabled, disableElevation, endIcon,
    fullWidth, variant, color }) => (
      <BaseButton id={id + IdSuFix}
        onClick={onClick} 
        className={clsx(classes.root, classes.styles)}
        startIcon={startIcon} disabled={disabled} disableElevation={disableElevation}
        endIcon={endIcon} fullWidth={fullWidth} variant={variant} color={color}>
        {children}
      </BaseButton>
  ));
