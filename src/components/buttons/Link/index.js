import React from 'react';
import PropTypes from 'prop-types';
import { StyledComponent } from './styles';

const IdSuFix = '.Link'
//
const Link = ({ children, ...props}) => {

  return ( 
    <StyledComponent
        id={props.id + IdSuFix}
        styles={props.styles}
        {...props}
    >
      {children}
    </StyledComponent>
  );
}

//
Link.propTypes = {
  children: PropTypes.node
}

export default Link;