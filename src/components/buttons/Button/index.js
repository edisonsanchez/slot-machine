import React from 'react';
import PropTypes from 'prop-types';
import { StyledComponent } from './styles';

//
const Button = ({ children, ...props}) => {

  return ( 
    <StyledComponent
        styles={props.styles}
        {...props}
    >
      {children}
    </StyledComponent>
  );
}

//
Button.propTypes = {
  children: PropTypes.node
}

export default Button;