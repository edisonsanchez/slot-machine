import React from 'react';
// import { isMobile } from 'react-device-detect';

import { useLanguageContext } from 'contexts/LanguageContext'
import { TabMenu, TabMenuItem } from 'components/menus/tabs'
import { Stylesheet } from './styles';

const ID = '.TabBar';
//
export const TabBar = ({itemSelected = 'home', id, styles, ...props}) => {
  const [value, setValue] = React.useState(itemSelected);
  const { translate } = useLanguageContext()

  //
  const handleChange = (event, newValue) => {
    setValue(newValue);

    props.onChange(newValue)
  }

  ///Render
  return (
    <TabMenu id={`${id}${ID}`}
      styles={Stylesheet.menu}
      value={value} 
      onChange={handleChange}
      centered>
      <TabMenuItem id={`${id}${ID}:MENU.HOME`}
        styles={Stylesheet.item}
        value='home' 
        label={translate('MENU.HOME')}/>
      <TabMenuItem id={`${id}${ID}:MENU.SERVICES`}
        styles={Stylesheet.item}
        value='services' 
        label={translate('MENU.SERVICES')}/>
      <TabMenuItem id={`${id}${ID}:MENU.CONTACT`}
        styles={Stylesheet.item}
        value='contact' 
        label={translate('MENU.CONTACT')}/>
    </TabMenu>
  );
}

export default TabBar