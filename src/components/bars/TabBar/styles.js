import { isMobile } from 'react-device-detect';

export const Stylesheet = {
  menu: {
    root: {
      height: isMobile ? '5vh' : '7vh',
      width: '100%',
      backgroundColor: 'black',
      minHeight: isMobile? '5vh' : '7vh',
    }
  },

  item: {
    root: {
      backgroundColor: 'black',
      color: 'white',
      minHeight: isMobile? '5vh' : '7vh',
      width: '33%',
      fontSize: isMobile? 14 : '1.4vmax',
      minWidth: '1vmax',
      paddingLeft: '0.2vmax',
      paddingRight: '0.2vmax',
    }
  }


}