import React from 'react';
// import { isMobile } from 'react-device-detect';

// import { useLanguageContext } from 'contexts/LanguageContext'
import { View } from 'components/views';
// import Logo from 'components/media/images/Logo';
// import { Button } from 'components/buttons'
// import { TabBar } from 'components/bars/TabBar'
// import { LanguageBar } from 'components/bars/LanguageBar'
// import { Stylesheet } from './styles';

import { ButtonIconMenu } from 'components/buttons/presets/ButtonMenu';
import { DrawerMenu } from 'components/menus/drawers/DrawerMenu';

import { AppBar } from 'bases/menus/headers/AppBar'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

//
export const AdminBar = (props) => {
  const [ open, setOpen ] = React.useState(true);
  // const { translate } = useLanguageContext()

  ///Render
  // return isMobile ? (
  return (
    <View>
      <AppBar>
        <ButtonIconMenu 
          onClick={() => {console.log('Button Icon Menu Pressed'); setOpen(true)}}
          />
      </AppBar>
      <DrawerMenu open={open} onClose={() => setOpen(false)}>
        <List>
          {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </DrawerMenu>
    </View>
  )
  // : (
  //   <View>
  //     <AppBar>
  //       <ButtonIconMenu />
  //     </AppBar>
  //   </View>
  // )
}

export default AdminBar;