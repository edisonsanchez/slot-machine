import React from 'react';
import { isMobile } from 'react-device-detect';

import { useLanguageContext } from 'contexts/LanguageContext'
import { View, Row } from 'components/views';
import Logo from 'components/media/images/Logo';
import { Button } from 'components/buttons'
import { TabBar } from 'components/bars/TabBar'
import { LanguageBar } from 'components/bars/LanguageBar'
import { Stylesheet } from './styles';

const IdSuFix = '.NavBar';

//
export const NavBar = (props) => {
  const { translate } = useLanguageContext()

  ///Render
  return isMobile ?
  (
    <View>
      <Row id={`${IdSuFix}.Row{Stylesheet.container}`}
        styles={Stylesheet.container}>
        <View id={`${IdSuFix}.View{Stylesheet.logoView}`}
          styles={Stylesheet.logoView}>
          <Logo />
        </View>
        <View id={`${IdSuFix}.View{Stylesheet.actionsView}`}
          styles={Stylesheet.actionsView}>
          <LanguageBar />
          <Button id={'Login' + IdSuFix} 
            styles={Stylesheet.button}
            onClick={() => props.onChange('login')}>
            {translate( 'LOGIN.BUTTON' )}
          </Button>
        </View>
      </Row>
      <Row>
        <View id={`${IdSuFix}.View{Stylesheet.menuView}`}
          styles={Stylesheet.menuView}>
          <TabBar id={`${IdSuFix}.View.TabBar{Stylesheet.menu}`}
            style={Stylesheet.menu} 
            onChange={props.onChange}
            itemSelected={props.itemSelected}
            />
        </View>
      </Row>
    </View>
  )
  : (
    <View>
      <Row id={`${IdSuFix}.Row{Stylesheet.container}`}
        styles={Stylesheet.container}>
        <View id={`${IdSuFix}.View{Stylesheet.logoView}`}
          styles={Stylesheet.logoView}>
          <Logo />
        </View>
        <View id={`${IdSuFix}.View{Stylesheet.menuView}`}
          styles={Stylesheet.menuView}>
          <TabBar onChange={props.onChange}
            itemSelected={props.itemSelected}
          />
        </View>
        <View id={`${IdSuFix}.View{Stylesheet.actionsView}`}
          styles={Stylesheet.actionsView}>
          <LanguageBar />
          <Button id={'Login' + IdSuFix} 
            styles={Stylesheet.button}
            onClick={() => props.onChange('login')}>
            {translate( 'LOGIN.BUTTON' )}
        </Button>
        </View>
      </Row>
    </View>
  )
}

export default NavBar