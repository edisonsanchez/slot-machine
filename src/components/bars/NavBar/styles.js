import { isMobile } from 'react-device-detect';

export const Stylesheet = {
  container: {
    root: {
      height: '7vh',
      width: '100%',
    }
  },

  logoView: {
    root: {
      width: isMobile ? '50%' : '20%',
    }
  },

  menuView: {
    root: {
      width: isMobile ? '100%' : '60%',
      backgroundColor: 'black',
      justifyContent: 'flex-end',
    }
  },

  actionsView: {
    root: {
      padding: 10,
      width: isMobile ? '50%' : '20%',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    }
  },

  button: {
    root: {
      height: '100%',
      width: '80%',
      fontSize: isMobile ? '4vw' : 10,
      lineHeight: isMobile? '1.75' : '1',
      minWidth: 40,
    }
  },

  menu: {
    menu: {
      root: {
        backgroundColor: 'yellow',
      }
    },
    item: {
      root: {
        backgroundColor: 'blue',
      }
    }
  }
}