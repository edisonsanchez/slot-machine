import {isMobile} from 'react-device-detect';

export const Stylesheet = {
  container: {
    root: {
      
    }
  },

  button: {
    root: {
      minWidth: isMobile ? '4vw' : '3vh',
      width: '100%',
      justifyContent: 'flex-start',
      backgroundColor: 'transparent',
      color: 'black',
      padding: 0,
      // marginLeft: 5,
      marginRight: isMobile? 3 : '1vw',
    },

    label: {

    }
  },

  flag: {
    root: {
      width: isMobile ? '4vh' : '3.5vh',
      borderRadius: 5,
    }
  },

  tag: {
    root: {
      fontSize: isMobile? '5vmin' : 16,
      marginLeft: 5,
    }
  },

  menu: {
    root: {
      width: isMobile? '50vw' : '30vh',
    }
  },

  menuItem: {
    root: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: '100%',
    }
  },

  itemLabel: {
    root: {
      marginLeft: 5,
      fontSize: isMobile? '5vmin' : 16,
    }
  }
}