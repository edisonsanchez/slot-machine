import React from 'react'

import { useLanguageContext } from 'contexts/LanguageContext'
import { languages as RawLanguages } from 'data/languages'
import { View } from 'components/views';
import { Button } from 'components/buttons';
import { Img } from 'components/media/images/Img';
import { Text } from 'components/typography/Text'
import { Stylesheet } from './styles';
import DropdownMenu from 'components/menus/dropdowns/DropdownMenu'
import DropdownMenuItem from 'components/menus/dropdowns/DropdownMenuItem'

const IdSuFix = '.LanguageBar';

//
export const LanguageBar = ( {id='', ...props}) => {
  const { language, translate, onChange } = useLanguageContext()
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [langObject, setLangObject] = React.useState(
    RawLanguages.find( (x) => { return x.id === language } ));

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //
  const changeLanguage = (id) => {
    console.log('changeLanguage');
    onChange(id)
    console.log(language)

    setLangObject(RawLanguages.find(x => x.id === id));
    setAnchorEl(null);
  }

  //Render Languages
  const renderLanguages = ( ) => {

    const ButtonStyle = props && props.style && props.style.button ? [Stylesheet.menuItem, ...props.style.button] : Stylesheet.menuItem;
    const ImageStyle = props && props.style && props.style.flag ? [Stylesheet.flag, ...props.style.flag] : Stylesheet.flag;
    const TextStyle = props && props.style && props.style.text ? [Stylesheet.itemLabel, ...props.style.text] : Stylesheet.itemLabel;


    return RawLanguages.map ( item => {
      return (
        language !== item.id &&
        <DropdownMenuItem key={item.id} id={props.id + '.LanguageBar.View.DropdownMenu.DropdownItem(Stylesheet.menuItem)'} 
          style={ButtonStyle}
          onClick={() => changeLanguage(item.id)}>
          <Img id={props.id + '.LanguageBar.View.DropdownMenu.DropdownItem.Img(Stylesheet.flag)'} 
            src={item.flag} 
            alt={'language-selected-' + item.label} 
            styles={ImageStyle}/>
            <Text id={props.id + IdSuFix} 
              styles={TextStyle}>{translate(item.label)}
            </Text>
        </DropdownMenuItem>
      )
    })
  }

  const BtnStyle = props && props.style && props.style.btn && props.style.btn.button 
    ? {...Stylesheet.button.root, ...props.style.btn.button} : Stylesheet.button;
  const BtnImageStyle = props && props.style && props.style.btn && props.style.btn.flag 
    ? {...Stylesheet.flag, ...props.style.btn.flag} : Stylesheet.flag;
  const TagStyle = props && props.style && props.style.btn && props.style.btn.tag 
    ? {...Stylesheet.tag, ...props.style.btn.tag} : Stylesheet.tag;

  // console.log('BtnImageStyle')
  // console.log(BtnImageStyle)
  // console.log(props)

  ///Render
  return (
    <View>
      <Button id={id + '.LanguageBar.View.Button(Stylesheet.button)'} 
        styles={BtnStyle}
        aria-controls="simple-menu" 
        aria-haspopup="true" 
        onClick={handleClick}>
        <Img id={props.id + '.LanguageBar.View.Button.Img(Stylesheet.flag)'} 
            src={langObject.flag} 
            alt={'language-selected-' + langObject.tag} styles={BtnImageStyle}/>
        { props.withLabel &&
          <Text id={id + '.LanguageBar.View.Button.Text(Stylesheet.label)'} 
            styles={TagStyle}>{translate(langObject.label)}</Text>
        }
      </Button>
      <DropdownMenu id={id + '.LanguageBar.View.DropdownMenu(Stylesheet.menu)'} 
        style={Stylesheet.menu}
        anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={() => handleClose()}>
        { renderLanguages() }
      </DropdownMenu>
    </View>
  )
}


//
LanguageBar.defaultProps = {
  id: ''
}

export default LanguageBar