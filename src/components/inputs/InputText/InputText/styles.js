import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/inputs/InputText';

const IdSuFix = '[C.InputText]'

// Local Styles
const styles = {
  root: {

  },

  input: {

  }
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    input:  props => ( props.styles && props.styles.input 
      ? { ...styles.input,
          ...props.styles.input 
      } : styles.input),

    empty: {}

  })
  (({ classes, children, id, ...props }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root, classes.fixed)}
      {...props}>
      {children}
    </BaseComponent>
  ));
