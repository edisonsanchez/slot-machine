import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.InputText';

//Functional Class Component
export const InputText = ({id, ...props}) => {
  return (
    <StyledComponent id={id + IdSuFix} {...props} />
  )
}

//
InputText.propTypes = {
  id: PropTypes.string
}
//
InputText.defaultProps = {
  id: IdSuFix
}
  
  export default InputText;