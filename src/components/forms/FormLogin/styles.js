import React from 'react';
import clsx from 'clsx';
import { isMobile } from 'react-device-detect';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/forms/Form';

const IdSuFix = '[C.Form]'


export const Stylesheet = {
  error: {
    root: {
      color: 'red',
      fontWeight: 600,
      width: '100%',
      justifyContent: 'center',
      fontSize: isMobile? '4vw' : '1.5vw',
      textDecoration: 'underline'
    }
  }
}

// Local Styles
const styles = {
  root: {
    height: '100%',
    justifyContent: 'space-evenly',
  },
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    empty: {}

  })
  (({ classes, children, id, ...props }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root, classes.fixed)}
      {...props}>
      {children}
    </BaseComponent>
  ));
