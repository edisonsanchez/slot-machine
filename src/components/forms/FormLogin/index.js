import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent as Form, Stylesheet } from './styles'
import { InputText } from 'components/inputs'
import { Button, Link } from 'components/buttons'
import { Text } from 'components/typography';
import { View } from 'components/views'
import { H1 } from 'components/typography'
import { useLanguageContext } from 'contexts/LanguageContext'
//
const IdSuFix = '.FormLogin';

//Functional Class Component
export const FormLogin = ({id, styles, username, password, onSubmit, onForgot, loading, disabled, ...props}) => {
  const {translate} = useLanguageContext();


  const stylesForm = styles && styles.form ? styles.form : {};
  const stylesViewInputs = styles && styles.viewInputs ? styles.viewInputs : {};
  const stylesInput = styles && styles.input? styles.input : {};
  const stylesViewLink = styles && styles.viewLink ? styles.viewLink : {};
  const stylesLink = styles && styles.link ? styles.link : {};
  const stylesViewButton = styles && styles.viewButton ? styles.viewButton : {};
  const stylesButton = styles && styles.button ? styles.button : {};


  console.log(`Loading: ${loading}`)
  console.log(`disabled: ${disabled}`)
  //
  return (
    <Form id={id + IdSuFix} styles={stylesForm} onSubmit={onSubmit}>
      <View styles={stylesViewInputs}>
        <H1>{translate( 'LOGIN.TITLE' )}</H1>
        <InputText id={'Username' + IdSuFix} {...username} styles={stylesInput}/>
        <InputText id={'Password' + IdSuFix} {...password} styles={stylesInput}/>
        { props.error && <Text styles={Stylesheet.error}>{props.error}</Text>}
      </View>
      <View styles={stylesViewLink}>
        { !disabled &&
          <Link id={'Forgot' + IdSuFix} onClick={onForgot} styles={stylesLink}>
            {translate( 'LOGIN.FORGOT' )}
          </Link>
        }
      </View>
      <View styles={stylesViewButton}>
        <Button id={'Login' + IdSuFix} styles={stylesButton} onClick={onSubmit} disabled={disabled}>
          {translate( loading ? 'loading' : 'LOGIN.BUTTON' )}
        </Button>
      </View>
    </Form>
  )
}

//
FormLogin.propTypes = {
  id: PropTypes.string,
}
//
FormLogin.defaultProps = {
  id: IdSuFix
}
  
  export default FormLogin;