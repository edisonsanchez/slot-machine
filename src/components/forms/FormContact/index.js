import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent as Form } from './styles'
import { InputText } from 'components/inputs'
import { Button } from 'components/buttons'
import { View } from 'components/views'
import { H1, Text } from 'components/typography'
import { useLanguageContext } from 'contexts/LanguageContext'
import { useInputValue } from 'hooks/useInputValue';

//
const IdSuFix = '.FormLogin';

//Functional Class Component
export const FormLogin = ({id, styles }) => {
  const {translate} = useLanguageContext();
  const email = useInputValue('', 'email', translate( 'LANDING.CONTACT.EMAIL' ));
  const name = useInputValue('', 'string', translate( 'LANDING.CONTACT.NAME' ));
  const phone = useInputValue('', 'phone', translate( 'LANDING.CONTACT.PHONE' ));
  const description = useInputValue('', '', translate( 'LANDING.CONTACT.DESCRIPTION' ));

  const stylesForm = styles && styles.form ? styles.form : {};
  const stylesViewInputs = styles && styles.viewInputs ? styles.viewInputs : {};
  const stylesInput = styles && styles.input? styles.input : {};
  // const stylesViewLink = styles && styles.viewLink ? styles.viewLink : {};
  // const stylesLink = styles && styles.link ? styles.link : {};
  const stylesViewButton = styles && styles.viewButton ? styles.viewButton : {};
  const stylesButton = styles && styles.button ? styles.button : {};

  //
  const handleSubmit = (event) => {
    event.preventDefault();

    console.log(`Send message to ${email.value} ${name} phone:${phone} with description: ${description}`)
  }

  //
  return (
    <Form id={id + IdSuFix} styles={stylesForm} onSubmit={handleSubmit}>
      <View styles={stylesViewInputs}>
        <H1>{translate( 'LANDING.CONTACT.TITLE' )}</H1>
        <Text>{translate( 'LANDING.CONTACT.DESCRIPTION' )}</Text>
        <InputText id={'Email' + IdSuFix} {...email} styles={stylesInput}/>
        <InputText id={'Name' + IdSuFix} {...name} styles={stylesInput}/>
        <InputText id={'Phone' + IdSuFix} {...phone} styles={stylesInput}/>
        <InputText id={'Description' + IdSuFix} {...description} styles={stylesInput}/>
      </View>
      <View styles={stylesViewButton}>
        <Button id={'Send' + IdSuFix} styles={stylesButton}>
          {translate( 'LANDING.CONTACT.BUTTON' )}
        </Button>
      </View>
    </Form>
  )
}

//
FormLogin.propTypes = {
  id: PropTypes.string,
}
//
FormLogin.defaultProps = {
  id: IdSuFix
}
  
  export default FormLogin;