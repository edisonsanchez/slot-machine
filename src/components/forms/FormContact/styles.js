import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/forms/Form';

const IdSuFix = '[C.Form]'

// Local Styles
const styles = {
  root: {
    height: '100%',
    justifyContent: 'space-evenly',
  },
};

//
export const StyledComponent = withStyles({
    root:  props => ( props.styles && props.styles.root 
      ? { ...styles.root,
          ...props.styles.root 
      } : styles.root),

    empty: {}

  })
  (({ classes, children, id }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root, classes.fixed)}>
      {children}
    </BaseComponent>
  ));
