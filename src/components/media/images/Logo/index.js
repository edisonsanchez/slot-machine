import React from "react"
import Img from 'components/media/images/Img'
import {Stylesheet} from './styles'
import LogoPNG from 'assets/logo/Logo.png'

const xID = '.Logo';


export const Logo = ({style}) => {

  const Styles = { ...Stylesheet.logo, ...style };

  return (
    <Img id={`${xID}`}
      style={Styles}
      src={LogoPNG}
      alt={'SlotMachines Administrative - Logo PNG'}
      />
  )
}

export default Logo
