import React from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import BaseComponent from 'bases/images/Img';

const IdSuFix = '[C.Img]'

// Local Styles
const styles = {
  root: {
  },
};

//Classes Component
export const StyledComponent = withStyles({
  root:  props => ( props.styles && props.styles.root 
    ? { ...styles.root, ...props.styles.root } : styles.root),

  empty: {}

  })
  (({ classes, id, ...props }) => (
    <BaseComponent id={id + IdSuFix}
      className={clsx(classes.root)} {...props}/>
  ));
