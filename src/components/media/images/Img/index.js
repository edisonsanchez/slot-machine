import React from 'react'
import PropTypes from 'prop-types'
import { StyledComponent } from './styles'

//
const IdSuFix = '.Img';

//Functional Class Component
export const Img = ({id, styles, ...props}) => {
  // console.log(props);
  return (
    <StyledComponent id={id + IdSuFix} styles={styles} {...props} />
  )
}

//
Img.propTypes = {
  children: PropTypes.node,
}
//
Img.defaultProps = {
  id: IdSuFix
}
  
  export default Img;