import { isMobile } from 'react-device-detect';


// Local Styles
export const Stylesheet = {
  slide: {
    root: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      fill: 'white',
      backgroundSize: 'cover',
      
    }
  },

  holder: {
    root: {
      // height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: '2vh',
      paddingBottom: '2vh',
      backgroundColor: 'rgba(0,0,0,0.5)',
      width: '100%',
      color: 'white',
      fill: 'white',

    }
  },

  content: {
    root: {
      // height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: 'blue',
      width: '65%',
    }
  },

  previousButton: {
    color: 'white',
    fill: 'white'
  },

  button: {
    root: {
      width: isMobile ? '40vw' : '20vmax',
      marginTop: '2vh',
    }
  },

  title: {
    root: {
      fontSize: isMobile? '4vh' : '3vmax',
      marginBottom: '1vh',
    }
  },


  description: {
    root: {
      fontSize: '2vmax',
    }
  }


};

