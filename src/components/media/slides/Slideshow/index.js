import React from 'react'
import PropTypes from 'prop-types'
import { isMobile } from 'react-device-detect';
import { useLanguageContext } from 'contexts/LanguageContext'
import { View } from 'components/views';
import { H1, Text } from 'components/typography';
import { Button } from 'components/buttons';
import { Stylesheet } from './styles'
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
// import 'react-animated-slider/build/vertical.css';

import './styles.css'

const content = [
  {
    title: 'SLIDE.HOME.TITLE',
    description: 'SLIDE.HOME.DESCRIPTION',
    image: {
      portrait: require('assets/images/slot-machines-portrait-01.jpg'),
      landscape: require('assets/images/slot-machines-landscape-01.jpg')
    },
    button: 'SLIDE.HOME.BUTTON'
  },
  {
    title: 'SLIDE.SECOND.TITLE',
    description: 'SLIDE.SECOND.DESCRIPTION',
    image: {
      portrait: require('assets/images/slot-machines-portrait-02.jpg'),
      landscape: require('assets/images/slot-machines-landscape-02.jpg'),
    },
    button: 'SLIDE.SECOND.BUTTON'
  },
  {
    title: 'SLIDE.THIRD.TITLE',
    description: 'SLIDE.THIRD.DESCRIPTION',
    image: {
      portrait: require('assets/images/slot-machines-portrait-03.jpg'),
      landscape: require('assets/images/slot-machines-landscape-03.jpg')
    },
    button: 'SLIDE.THIRD.BUTTON'
  },
];

//Functional Class Component
export const Slideshow = ({children, id, styles, ...props}) => {
  const { translate } = useLanguageContext();

  //
  const renderItem = ( item, index ) => {
    return (
      <div
        id={'View.Slide'}
        key={index}
        style={isMobile? {background: `url('${item.image.portrait}')`, ...Stylesheet.slide.root }:
        {background: `url('${item.image.landscape}')`, ...Stylesheet.slide.root } }>
          <View
            id={'View.Holder'}
            styles={Stylesheet.holder}
            className="center">
            <View 
              id={'View.Content'}
              styles={Stylesheet.content}
              className="center">
              <H1 styles={Stylesheet.title}>{translate(item.title)}</H1>
              <Text styles={Stylesheet.description}>{translate(item.description)}</Text>
              <Button styles={Stylesheet.button}>{translate(item.button)}</Button>
            </View>
          </View>
      </div>
    );
  }

  //
  return (

    <Slider id={'SlideShow'}
      // style={{backgroundColor: 'white', fill: 'white'}}
      // styles={{fill: 'white'}}
      classNames={Stylesheet.previousButton}
    // autoplay={3000}
    >
      {content.map((item, index) => (
        renderItem(item, index)
      ))}
    </Slider>
  )
}

//
Slideshow.propTypes = {
  id: PropTypes.string 
}
//
Slideshow.defaultProps = {
  id: ''
}
  
  export default Slideshow;