import { useRef, useEffect } from 'react';

//First, we create a custom hook that takes
//in a value. Then we use the useRef hook to 
//create a ref for the value. Finally, we use 
//useEffect to remember the latest value.
export const usePrevious = value => {
  const ref = useRef();

  useEffect ( () => {
    ref.current = value;  
  });

  return ref.current;
}

export default usePrevious;