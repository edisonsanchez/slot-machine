/* eslint-disable react-hooks/exhaustive-deps */
import {useState, useLayoutEffect} from 'react'

//This hook is a small example of how to execute 
//a callback right after a component is mounted. 
//For the second argument, we simply use useEffect 
//with an empty array, to execute the provided 
//callback once as soon as the component is mounted.

const useComponentDidMount = onMountHandler => {
  const [isMounted, setState] = useState(false);

  useLayoutEffect(() => {
    if (!isMounted) {
      setState(true);
      onMountHandler();
    }
  }, []);

  return {isMounted};
};

export default useComponentDidMount;