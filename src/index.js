import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import App from './App';
//Load Languages for Moment Library
import 'moment/locale/es';
import 'moment/locale/pt';
import 'moment/locale/fr';
//Load Contexts
import { LanguageContextProvider } from "contexts/LanguageContext";
import { AuthContextProvider } from "contexts/AuthContext";

//Debugger on Development Directly on Console Browser
if (process.env.NODE_ENV !== 'production') {
  localStorage.setItem('debug', 'slot-machine-app:*');
}

ReactDOM.render(
  <AuthContextProvider>
    <React.Suspense fallback="loading...">
      <LanguageContextProvider>
        <Router>
          <App />
        </Router>
      </LanguageContextProvider>
    </React.Suspense>
  </AuthContextProvider>
  ,
  document.getElementById('app')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
