import React from 'react';
import { Stylesheet } from './styles';
import { View } from 'components/views';
import FormContact from 'components/forms/FormContact';

//Component
const ContactPage = (props) => {
  // const [body, setBody] = React.useState('home');

  //Render
  return (
    <View id={'CONTACT.View{Sylesheet.View}'}
      styles={Stylesheet.container}>
      <FormContact />
    </View>

  );
};

const Contact = React.memo(ContactPage);
export default Contact;
