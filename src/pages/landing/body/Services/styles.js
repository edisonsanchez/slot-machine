import { isMobile } from 'react-device-detect'

//
export const Stylesheet = {

  container : {
    root: {
      // height: isMobile ? '88vh' : '93vh',
      // backgroundColor: 'red',
      justifyContent: 'flex-start',
      alignItems: 'center',
    }
  },

  title : {
    root: {
      marginTop: 10,
      marginBottom: 10,
      fontSize: isMobile? '3vh' : 16,

    }
  },

  description : {
    root: {
      marginBottom: 10,
      marginLeft: '10vw',
      marginRight: '10vw',
      fontSize: isMobile? '4vw' : 12,
      textAlign: 'center',
    }
  },

};