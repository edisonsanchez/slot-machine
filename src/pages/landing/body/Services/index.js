import React from 'react';
import { Stylesheet } from './styles';
import { View } from 'components/views';
import { H1, Text } from 'components/typography';
import { useLanguageContext } from 'contexts/LanguageContext'

//Component
const ServicesPage = (props) => {
  const { translate } = useLanguageContext()


  //Render
  return (
    <View id={'SERVICES.View{Sylesheet.View}'}
      styles={Stylesheet.container}>
      <H1 id={'SERVICES.View{Sylesheet.title}'}
        styles={Stylesheet.title}>
        {translate('LANDING.SERVICES.TITLE')}
      </H1>
      <Text id={'SERVICES.View{Sylesheet.description}'}
        styles={Stylesheet.description}>
        {translate('LANDING.SERVICES.DESCRIPTION')}
      </Text>
      {/* <Divider /> */}
    </View>

  );
};

const Services = React.memo(ServicesPage);
export default Services;
