import React from 'react';
import { Stylesheet } from './styles';
import LandingLayout from 'layouts/LandingLayout';
import { View } from 'components/views';
import NavBar from 'components/bars/NavBar';
import Slideshow from 'components/media/slides/Slideshow';
import Services from 'pages/landing/body/Services';
import Contact from 'pages/landing/body/Contact';
import ButtonPWA from 'components/buttons/ButtonPWA';
import { useHistory } from "react-router-dom";


//Component
const HomePage = (props) => {
  const [body, setBody] = React.useState('home');
  const history = useHistory();


  //
  const handleChange = (id) => {

    console.log(`HomePage -> handleChangge(${id})`);
    console.log(id);

    switch(id) {
      case 'home':
      case 'services':
      case 'contact':
        setBody(id);
        break;

      case 'login':
        history.push('/login');
        break;

      default:
        break;
    }
  }

  //Render
  return (
    <LandingLayout metaTitle={'Home'}>
      <View id={'HOME.LandingLayout.View{Sylesheet.header}'}
        styles={Stylesheet.header}>
        <NavBar id={'HOME.LandingLayout.View.NavBar'}
          type='landing'
          onChange={(id) => handleChange(id)}
          itemSelected={props.itemSelected}
          />
        <View id={'HOME.LandingLayout.View{Sylesheet.body}'}
          styles={Stylesheet.body}>
          { body === 'home' && <Slideshow onClick={(id) => {handleChange(id)}}/> }
          { body === 'services' && <Services /> }
          { body === 'contact' && <Contact /> }
        </View>
      </View>
      <ButtonPWA />
    </LandingLayout>
  );
};

const Home = React.memo(HomePage);
export default Home;
