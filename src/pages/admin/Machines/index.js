import React from 'react';
// import { Stylesheet } from './styles';
import AdminLayout from 'layouts/AdminLayout';
// import { View } from 'components/views';


//Component
const MachinesPage = (props) => {
  // const [body, setBody] = React.useState('machines');

  //Render
  return (
    <AdminLayout metaTitle={'Machines'}>
      Body Machines
    </AdminLayout>
  );
};

const Machines = React.memo(MachinesPage);
export default Machines;
