import { isMobile } from 'react-device-detect'

//
export const Stylesheet = {
  container: {
    root: {

    }
  },

  body: {
    root: {
      height: isMobile ? '88vh' : '93vh',
      backgroundColor: 'transparent',
    }
  },

};