import { isMobile } from 'react-device-detect';
//
export const Stylesheet = {
  formHeader: {
    root: {
      width: '100%',
      height: isMobile? '20%' : '10%',
      justifyContent: 'flex-end',
    }
  },

  headerButtons: {
    root: {
      width: '100%',
      justifyContent: 'space-between',
    }
  },

  linkClose : {
    root: {
      height: isMobile? '100%' : '100%',
      fontSize: isMobile? '5vmin' : 16,
      marginRight: 5,
      color: 'black',
    }
  },

  formBody: {
    form: {
      root: {
        backgroundColor: 'gray',
        width: '90%',
        height: isMobile? '60%' : '80%',
        borderRadius: 5,
        paddingLeft: '5%',
        paddingRight: '5%',
      }
    },
    viewInputs: {
      root: {
        height: '65%',
        justifyContent: 'space-around',

      }
    },
    viewLink: {
      root: {
        height: '15%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: isMobile? '2vmin' : '1vmax',
      }
    },
    viewButton: {
      root: {
        height: '20%',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }
    },
    link: {
      root: {
        // backgroundColor: 'blue',
        // width: '100%',
        height: '100%',
        fontSize: isMobile? '3vw' : '2vh'
      }
    },
    button: {
      root: {
        // backgroundColor: 'blue',
        // width: '100%',
        // height: '70%',
      }
    },

  },

  formFooter: {
    root: {

      width: '100%',
      height: isMobile? '20%' : '10%',
      paddingTop: '2%',
      justifyContent: 'center',
      alignItems: 'flex-start',
    }
  },


  error: {
    root: {
      color: 'red',
      fontWeight: 600,
      width: '100%',
      justifyContent: 'center',
      fontSize: '1.5vw',
      textDecoration: 'underline'
    }
  }
};