import React from 'react'
import { useHistory } from "react-router-dom";
// import { Stylesheet } from './styles'
import { LoginLayout } from 'layouts'
import { View, Row } from 'components/views'
import FormLogin from 'components/forms/FormLogin'
import useInputValue from 'hooks/useInputValue'
import useErrorMessage from 'hooks/useErrorMessage'
import { Link } from 'components/buttons'
import LanguageBar from 'components/bars/LanguageBar'
import {useLanguageContext} from 'contexts/LanguageContext';
import {useAuthContext} from 'contexts/AuthContext';
import { Stylesheet } from './styles'

//Component
const LoginPage = (props) => {
  const history = useHistory();
  const {translate} = useLanguageContext();
  const [ isLoading, setLoading ] = React.useState(true);
  const [ isSubmitting, setSubmitting ] = React.useState(false);
  const { authLogin } = useAuthContext();

  //
  const username = useInputValue('', 'email', translate( 'LOGIN.USERNAME' ));
  const password = useInputValue('', 'password', translate( 'LOGIN.PASSWORD' ));
  const error = useErrorMessage();

  //ComponentDidMount
  React.useLayoutEffect(() => {
    setLoading(false);
    //
    return function cleanup() {}
  }, []);

  // Submit button on Login Form.
  const handleSubmit = async () => {
    setSubmitting(true);

    console.log('handleSubmit');
    console.log('username');
    console.log(username);

    const result = await authLogin( username.value, password.value);

    console.log('result');
    console.log(result);

    if(result.data){
      history.push('/dashboard');
      return;
    } else if(result.error === 'UserNotConfirmedException'){
      history.push({pathname: "/confirm", state: { username: username, password: password }});
      return;
    }

    console.log('result.error.code');
    console.log(result.error);
    error.setMessage(translate(result.error));
    setSubmitting(false);
  }

  //
  const handleReturn = () => {
    history.push('/');
    return;
  }

  //Render
  return (
    <LoginLayout metaTitle={'Login'}>
      <View styles={Stylesheet.formHeader}>
        <Row styles={Stylesheet.headerButtons}>
          <LanguageBar withLabel/>
          <Link id={'Login-Close'} styles={Stylesheet.linkClose} 
            onClick={ () => handleReturn()}>
            {translate('LOGIN.CANCEL')}
          </Link>
        </Row>
      </View>
      <FormLogin styles={Stylesheet.formBody}
        username={username} password={password}
        onSubmit={handleSubmit}
        disabled={isSubmitting}
        error={error.message}
        loading={isSubmitting || isLoading }
        />
      <Row styles={Stylesheet.formFooter}>
        <Link id={'Login-Forgot'} styles={Stylesheet.linkSignup}
          onClick={ () => console.log('Press Signup')}>
          {translate( 'LOGIN.SIGNUP' )}
        </Link>
      </Row>
    </LoginLayout>
  );
};

const Login = React.memo(LoginPage);
export default Login;
