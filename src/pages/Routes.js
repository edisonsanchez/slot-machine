import React from 'react'
import { Route, Switch } from "react-router-dom";

// Routes
import Home from './landing/Home'
import { NotFound } from './NotFound'
import AuthenticatedRoute from "./AuthenticatedRoute";
import UnauthenticatedRoute from "./UnauthenticatedRoute";
//
import Login from './login/Login'
import Signup from './login/Signup'
import Confirm from './login/Confirm'
import Forgot from './login/Forgot'
//
import Store from './store/Store'
//
import Admin from './admin/Admin'
import Dashboard from './admin/Dashboard'
import Machines from './admin/Machines'


export default () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <UnauthenticatedRoute path="/login">
        <Login />
      </UnauthenticatedRoute>
      <UnauthenticatedRoute path="/signup">
        <Signup />
      </UnauthenticatedRoute>
      <UnauthenticatedRoute path="/confirm">
        <Confirm />
      </UnauthenticatedRoute>
      <UnauthenticatedRoute path="/forgot">
        <Forgot />
      </UnauthenticatedRoute>
      <AuthenticatedRoute path="/store">
        <Store />
      </AuthenticatedRoute>
      <AuthenticatedRoute path="/admin">
        <Admin />
      </AuthenticatedRoute>
      <AuthenticatedRoute path="/dashboard">
        <Dashboard />
      </AuthenticatedRoute>
      <AuthenticatedRoute path="/machines">
        <Machines />
      </AuthenticatedRoute>

      {/* Finally, catch all unmatched routes */}
      <Route>
        <NotFound />
      </Route>
    </Switch>
  )
}
