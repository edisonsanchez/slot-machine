//
export const onErrorAuth = (error) => {
  let message = 'Error!!!';

  if(error){
    message = error.toString();
  }

  alert(message);
}

//
export const onError = (error) => {
  let message = 'Error!!!';

  if(error){
    message = error.toString();
  }

  // Auth errors
  if (!(error instanceof Error) && error.message) {
    message = error.message;
  }

  alert(message);
}