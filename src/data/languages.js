// import { toAbsoluteUrl } from "../helpers";


export const languages = [
  {
    "id": "es",
    "tag": "spanish",
    "label": "LANGUAGE.SPANISH",
    "short": "spa",
    "abrev": "sp",
    "icon": "ES",
    "flag": require('assets/svg/flags/016-spain.svg'),
    "countryCode": "ES"
  },
  {
    "id": "ht",
    "tag": "creole",
    "label": "LANGUAGE.CREOLE",
    "short": "cre",
    "abrev": "ht",
    "icon": "CR",
    "flag": require('assets/svg/flags/185-haiti.svg'),
    "countryCode": "HT"
  },
  {
    "id": "en",
    "tag": "english",
    "label": "LANGUAGE.ENGLISH",
    "short": "eng",
    "abrev": "en",
    "icon": "US",
    "flag": require('assets/svg/flags/226-united-states.svg'),
    "countryCode": "US"
  },
  {
    "id": "fr",
    "tag": "french",
    "label": "LANGUAGE.FRENCH",
    "short": "fra",
    "abrev": "fr",
    "icon": "FR",
    "flag": require('assets/svg/flags/019-france.svg'),
    "countryCode": "FR"
  },
  {
    "id": "pr",
    "tag": "portugiese",
    "label": "LANGUAGE.PORTUGUESE",
    "short": "por",
    "abrev": "pr",
    "icon": "PR",
    "flag": require('assets/svg/flags/255-brazil.svg'),
    "countryCode": "BR"
  }
];

export const dataLanguage = {
  es: languages[0], 
  en: languages[1], 
  fr: languages[2], 
  pr: languages[3], 
};