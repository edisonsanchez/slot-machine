const constants = {
  appName: 'Slot Machines',
  appDescription: 'Slot Machines application, management system',
  metaTitle: 'Slot Machines',
  metaDescription: 'Slot Machines Management System'
}

export default constants;