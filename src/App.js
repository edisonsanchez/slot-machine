import React from 'react';
import './App.css';
import Log from 'functions/Log'
import Routes from './pages/Routes'

const App = () => {
  Log.info("App", `App.js`)

  return (
    <React.Suspense fallback={null}>
      <Routes />
    </React.Suspense>
  );
}

export default App;
