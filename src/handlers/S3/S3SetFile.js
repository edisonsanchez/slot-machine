import { Storage } from "aws-amplify";

export const S3SetFile = async ( file, filename, directory ) => {
  try{
    const data = await Storage.vault.put(filename, file, { contentType: file.type });

    return {
      data,
      error: false,
      loading: false
    }

  } catch (err) {
    return {
      data: null, 
      error: err,
      loading: false
    }
  }
}

//
export const S3SetFileUserDate = async (file, username) => {
  const filename = username 
    ?`${username}-${file.name}-${Date.now()}` 
    : `${file.name}-${Date.now()}`;

  return S3SetFile( file, filename );
}

export const S3SetFileUser = async (file, username) => {

  const filename = username 
    ? `${username}-${file.name}` 
    : `${file.name}`;

  return S3SetFile( file, filename );
}
