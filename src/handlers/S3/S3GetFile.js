import { Storage } from "aws-amplify";

export const S3GetFile = async (filename, directory) => {
  try{
    const data = await Storage.vault.get(filename);

    return {
      data,
      error: null, 
      loading: false
    };
  } catch (err){
    return {
      data: null,
      error: err, 
      loading: false
    };
  }
}

export const S3GetFilesUser = async (username, directory) => {
    const filename = username;

    return S3GetFile(filename, directory);
}