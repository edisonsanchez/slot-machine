import { cognitoConfirm } from './cognitoConfirm';
import { cognitoForgot } from './cognitoForgot';
import { cognitoSignin } from './cognitoSignin';
import { cognitoSignup } from './cognitoSignup';
import { federatedSignin } from './federatedSignin';

export { cognitoConfirm, cognitoForgot, cognitoSignin, cognitoSignup, federatedSignin };