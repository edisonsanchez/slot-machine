import {Auth} from 'aws-amplify';

export const cognitoSignup = async (username, password) => {
  let ret = {
    loading: true,
    data: null,
    error: null
  };

  //
  try {
    const user = await Auth.signUp(username.toLowerCase(), password);

    return {
      loading: false,
      data: user,
      error: null
    };
  } catch (err) {
    ret.loading=false;
    ret.error = {
      code: err.code,
      message: 'HANDLER.COGNITO.EXCEPTION.' + err.code.toUpperCase(),
      error: err
    }

    return ret;
  }
};
