import { Auth } from 'aws-amplify';

export const cognitoConfirm = async (username, confirmationCode) => {
  let ret = {
    loading: true,
    data: null,
    error: null,
  };

  try {
    const data = await Auth.confirmSignUp(username.toLowerCase(), confirmationCode);

    return {
      data: data,
      error: null,
      loading: false
    };
  } catch (err) {
    ret.loading= false;
    ret.error = {
      code: err.code,
      message: 'HANDLER.COGNITO.EXCEPTION.' + err.code.toUpperCase(),
      error: err
    }
    return ret;
  }
};

export default cognitoConfirm;