import {Auth} from 'aws-amplify';

export const cognitoSignin = async (username, password, option, userObject) => {

  try {
    let user;

    if(option === 'NEW_PASSWORD_REQUIRED'){
      user = await Auth.completeNewPassword(
        userObject, // the Cognito User Object
        password, // the new password
      );

      //New Password configured successfully
      return {
        loading: false,
        data: user,
        flow: null,
        error: null,
      };
    } else {
      const userUsername = username.toLowerCase();
      console.log(userUsername);

      user = await Auth.signIn(userUsername, password);

      if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
        //Login Success but you must change your password
        return {
          loading: false,
          data: user,
          error: null,
          flow: 'NEW_PASSWORD_REQUIRED',
        };
      } 

      //Login Success
      return {
        loading: false,
        data: user,
        error: null,
        flow: null,
      };
    }
  } catch (err) {
    //Login Error or NewPassword Error.
    if(option === 'NEW_PASSWORD_REQUIRED'){
      return {
        loading: false,
        data: null,
        error: {
          code: 'error',
          message: 'HANDLER.COGNITO.EXCEPTION.' + err.code.toUpperCase(),
          error: err
        },
        flow: 'NEW_PASSWORD_REQUIRED',
      };
    } 

    return {
      loading: false,
      data: null,
      error: {
        code: 'error',
        message: err.code,
        error: err
      },
      flow: null,
    };
  }
}
