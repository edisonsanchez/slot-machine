import { Auth } from 'aws-amplify';

export const cognitoForgot = async (email, password, code) => {
  let ret = {
    loading: true,
    data: null,
    error: null,
  };

  //
  try {
    let user;

    if(code && password){
      user = await Auth.forgotPasswordSubmit(email.toLowerCase(), code, password);

      return {
        loading: false,
        data: user || code,
        error: null,
        flow: code ? 'NEW_PASSWORD_REQUIRED' : null,
      };
    } else {
      user = await Auth.forgotPassword(email.toLowerCase());

      return {
        loading: false,
        data: user,
        error: null,
      };
    }
  } catch (err) {
    ret.loading= false;
    ret.error = {
      code: err.code,
      message: 'HANDLER.COGNITO.EXCEPTION.' + err.code.toUpperCase(),
      error: err
    }

    return ret;
  }
};
