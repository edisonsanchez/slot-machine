import {Auth} from 'aws-amplify';

export const federatedSignin = async (type) => {
  let ret = {
    loading: true,
    data: null,
    error: null
  };

  try {
    const user = await Auth.federatedSignIn({provider: type});

    return {
      loading: true,
      data: user,
      error: null
    };

  } catch (err) {
    ret.loading = false;
    ret.error = {
      code: err.code,
      message: 'HANDLER.COGNITO.EXCEPTION.' + err.code.toUpperCase(),
      error: err
    }

    return ret;
  }
};
