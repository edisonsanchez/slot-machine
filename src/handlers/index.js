import { cognitoConfirm } from './Cognito/cognitoConfirm';
import { cognitoForgot } from './Cognito/cognitoForgot';
import { cognitoSignin } from './Cognito/cognitoSignin';
import { cognitoSignup } from './Cognito/cognitoSignup';
import { federatedSignin } from './Cognito/federatedSignin';
//
import { S3SetFile, S3SetFileUserDate, S3SetFileUser } from './S3/S3SetFile'
import { S3GetFile, S3GetFilesUser } from './S3/S3GetFile'

//
export { 
  cognitoConfirm, cognitoForgot, cognitoSignin, cognitoSignup, federatedSignin,
  S3SetFile, S3SetFileUserDate, S3SetFileUser,
  S3GetFile, S3GetFilesUser
};
