# MACHINES

## Columns

- ID
- Serial
- Model
- Store
- Counters
- Coin
- Balance
- Actions

## Actions

- Create a new Machine.
- Update a Machine.
- Options
  ** Lock/Unlock a Machine.
  ** Set/Reset Counters
- History
  ** Change Log.
  ** Machine Log.
- Balance
  ** Make a Payment.
  ** Make an Adjustment.
  \*\* Balance Log
