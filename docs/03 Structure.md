# Folders Source

mkdir -p src/{assets/{images,logo,audio,video},bases/{styles,texts/{Span,Paragraph},buttons/{Link,Button,Floating,ButtonGroup,Chip,ChipGroup,Rating,SpeedDial},forms/{Form,FormModal},images/{Avatar,Image,Badge},menus/{Tabs,Menu},containers/{Box,Container,Card,Column,Row,Grid,Skeleton},lists/{Accordion,List,Divider,Tree},pickers/{DatePicker},inputs{CheckBox,Select,Slider,Switch,TextField,TextArea,Dropdown},modals/{Drawer,Alert,Snackbar,Tooltip,Popper},progress/{Progress,Spinner},tables/{table,pagination}},contexts,components/{bars,texts,buttons,tables,views},containers/{auth,users,session},data,graphql,handlers,helpers,hooks/{localStorage,sessionStorage,inputs},layouts,pages/{Home,Login,Admin},panels/{home,login,admin},styles}

- assets
  -- images
  -- logo
  -- audio
  -- video
- bases
  -- Span
  -- Paragraph
  -- Link
  -- Button
  -- Form
  -- Avatar
  -- Image
  -- Tabs
  -- Menu
  -- Box
  -- Column
  -- Row
- contexts
  -- LanguageContext
  -- AuthContext
  -- AppContext
- components
  -- bars
  -- texts
  -- buttons
  -- tables
  -- views
- containers
- data
- graphql
- handlers
- helpers
- hooks
- layouts
- pages
- panels
- styles
