npm install @material-ui/core @material-ui/icons apollo-boost aws-amplify aws-sdk axios graphql graphql-tag i18next i18next-xhr-backend intersection-observer material-ui-image moment moment-timezone prop-types react-apollo react-device-detect react-helmet react-i18next react-icons react-moment react-router-dom serve styled-components

npm install cypress debug --save-dev

# Index.js - Add these lines above ReactDOM.render

if (process.env.NODE_ENV !== 'production') {
localStorage.setItem('debug', 'awesome-react-app:\*');
}

# Create Log Class in functions/Log

# Import Log and Log.info(loginInfo, `App Component`)
