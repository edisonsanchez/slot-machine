# AliasExistsException

This exception is thrown when a user tries to confirm the account with an email or phone number that has already been supplied as an alias from a different account. This exception tells user that an account with this email or phone already exists.

HTTP Status Code: 400

# CodeMismatchException

This exception is thrown if the provided code does not match what the server was expecting.

HTTP Status Code: 400

# ExpiredCodeException

This exception is thrown if a code has expired.

HTTP Status Code: 400

# InternalErrorException

This exception is thrown when Amazon Cognito encounters an internal error.

HTTP Status Code: 500

# InvalidLambdaResponseException

This exception is thrown when the Amazon Cognito service encounters an invalid AWS Lambda response.

HTTP Status Code: 400

# InvalidParameterException

This exception is thrown when the Amazon Cognito service encounters an invalid parameter.

HTTP Status Code: 400

# LimitExceededException

This exception is thrown when a user exceeds the limit for a requested AWS resource.

HTTP Status Code: 400

# NotAuthorizedException

This exception is thrown when a user is not authorized.

HTTP Status Code: 400

# ResourceNotFoundException

This exception is thrown when the Amazon Cognito service cannot find the requested resource.

HTTP Status Code: 400

# TooManyFailedAttemptsException

This exception is thrown when the user has made too many failed attempts for a given action (e.g., sign in).

HTTP Status Code: 400

# TooManyRequestsException

This exception is thrown when the user has made too many requests for a given operation.

HTTP Status Code: 400

# UnexpectedLambdaException

This exception is thrown when the Amazon Cognito service encounters an unexpected exception with the AWS Lambda service.

HTTP Status Code: 400

# UserLambdaValidationException

This exception is thrown when the Amazon Cognito service encounters a user validation exception with the AWS Lambda service.

HTTP Status Code: 400

# UserNotFoundException

This exception is thrown when a user is not found.

HTTP Status Code: 400
